local addon = KuiNameplates
local mod = addon:NewPlugin('Borders')

function mod:Show(frame)
	local unitType = UnitClassification(frame.unit);
	
	-- Check for an existing border on this recycled unit frame
	local existingBorder = nil;

	for _, child in ipairs({frame:GetChildren()}) do
		if(child:GetName() == self.borderName) then
			existingBorder = child;
		end
	end

	if(existingBorder == nil) then
		existingBorder = CreateFrame("Frame", self.borderName, frame);
		existingBorder:SetBackdrop({edgeFile = "interface/buttons/white8x8", edgeSize = 1});
	end

	-- Do not draw a border for neutral mobs
	if(UnitReaction(frame.unit, "player") == 4) then
		existingBorder:Hide();
		return;
	end

	-- Do not draw a border for players
	if UnitIsPlayer(frame.unit) then
		existingBorder:Hide();
		return;
	end

	if(existingBorder ~= nil) then
		if(unitType == "worldboss") then
			existingBorder:SetBackdropBorderColor(self.colors.worldboss[1], self.colors.worldboss[2], self.colors.worldboss[3], 1);
		elseif(unitType == "rareelite") then
			existingBorder:SetBackdropBorderColor(self.colors.rareelite[1], self.colors.rareelite[2], self.colors.rareelite[3], 1);
		elseif(unitType == "elite") then
			existingBorder:SetBackdropBorderColor(self.colors.elite[1], self.colors.elite[2], self.colors.elite[3], 1);
		elseif(unitType == "rare") then
			existingBorder:SetBackdropBorderColor(self.colors.rare[1], self.colors.rare[2], self.colors.rare[3], 1);
		elseif(unitType == "normal") then
			existingBorder:SetBackdropBorderColor(self.colors.normal[1], self.colors.normal[2], self.colors.normal[3], 1);
		else
			-- Unknown unit type, no border.
			existingBorder:SetBackdropBorderColor(0, 0, 0, 0);
		end
		existingBorder:SetAllPoints(frame.HealthBar);
		existingBorder:Show();
	end
end

function mod:Hide(frame)
	for _, child in ipairs({frame:GetChildren()}) do
		if(child:GetName() == self.borderName) then
			child:Hide();
			break;
		end
	end
end

function mod:OnEnable()
	self:RegisterMessage('Show');
    self:RegisterMessage('Hide');
    self.colors = {
        worldboss	= {1, 0, 1},
        elite  		= {0.957, 0.957, 0.259},
        rareelite 	= {0, 0.380, 1},
        rare   		= {0, 0.380, 1},
        normal   	= {1, 1, 1}
    }
    self.borderName = "EliteBorder";
end